Source: snowflake
Maintainer: Debian Privacy Tools Maintainers <pkg-privacy-maintainers@lists.alioth.debian.org>
Uploaders: Ruben Pollan <meskio@sindominio.net>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               dh-golang,
               golang-any,
               golang-github-google-uuid-dev,
               golang-github-gorilla-websocket-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-smartystreets-goconvey-dev,
               golang-github-stretchr-testify-dev,
               golang-github-xtaci-kcp-dev,
               golang-github-xtaci-smux-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-net-dev,
               golang-google-protobuf-dev,
               golang-goptlib-dev (>= 1.2.0),
               golang-refraction-networking-utls-dev,
               golang-github-pion-webrtc.v3-dev (>= 3.1.56-1~)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/pkg-privacy-team/snowflake
Vcs-Git: https://salsa.debian.org/pkg-privacy-team/snowflake.git
Homepage: https://snowflake.torproject.org
Rules-Requires-Root: no
XS-Go-Import-Path: git.torproject.org/pluggable-transports/snowflake.git

Package: snowflake-proxy
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: WebRTC pluggable transport for Tor (proxy)
 Snowflake helps users circumvent censorship by making a WebRTC
 connection to volunteer proxies. These proxies relay Tor traffic to a
 Snowflake bridge and then to through the Tor network.
 .
 This package provides the proxy.

Package: snowflake-client
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: WebRTC pluggable transport for Tor (client)
 Snowflake helps users circumvent censorship by making a WebRTC
 connection to volunteer proxies. These proxies relay Tor traffic to a
 Snowflake bridge and then to through the Tor network.
 .
 This package provides the client.

Package: golang-snowflake-dev
Architecture: all
Depends: golang-github-google-uuid-dev,
         golang-github-gorilla-websocket-dev,
         golang-github-prometheus-client-golang-dev,
         golang-github-prometheus-client-model-dev,
         golang-github-smartystreets-goconvey-dev,
         golang-github-xtaci-kcp-dev,
         golang-github-xtaci-smux-dev,
         golang-golang-x-crypto-dev,
         golang-golang-x-net-dev,
         golang-google-protobuf-dev,
         golang-goptlib-dev (>= 1.2.0),
         golang-refraction-networking-utls-dev,
         golang-github-pion-webrtc.v3-dev (>= 3.1.56-1~),
         ${misc:Depends},
Description: WebRTC pluggable transport for Tor (library)
 Snowflake helps users circumvent censorship by making a WebRTC
 connection to volunteer proxies. These proxies relay Tor traffic to a
 Snowflake bridge and then to through the Tor network.
 .
 This package provides golang library sources.
